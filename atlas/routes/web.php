<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $a = \AtlasQuery::eventEditions();

   $a = \AtlasQuery::setEvent("eve-1999e1eb-0adb-4a1b-9e1b-367025ebd81a")->exhibitingOrganisations();

    // $a = \AtlasMutation::setEvent("eve-1999e1eb-0adb-4a1b-9e1b-367025ebd81a")->addExhibitingOrganisation(
    // '
    //             exhibitingOrganisation: {
    //                 companyName: "Prueba paquete Atlas 4",
    //                 addressLine1: "Reforma 243 - 15",
    //                 countryCode: MEX,
    //                 administratorEmail: "test4@reedexpo.com",
    //                 package: BRONZE, # Default: BRONZE
    //                 stands:[{name: "test-1"}],
    //                 representedBrands:[{name: "Reed Exhibitions"}]
    //             }
    // '
    // );
   return response()->json($a);
});
